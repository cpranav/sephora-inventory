<?php

namespace Tests\Unit;

use App;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class InventoryRepositoryTest extends TestCase
{

    use DatabaseMigrations, DatabaseTransactions;

    /** @var App\Repositories\InventoryRepository $inventoryRepository */
    private $inventoryRepository;

    public function setUp()
    {
        parent::setUp();

        $this->inventoryRepository = App::make('App\Repositories\InventoryRepository');
    }

    public function tearDown()
    {
        parent::tearDown();
    }


    /** @dataProvider increment_inventory_levels_data_provider */
    public function test_it_increments_inventory_levels_correctly($initial, $incrementBy, $expected)
    {
        $product = factory(App\Models\Product::class)->create();
        $distributionCenter = factory(App\Models\DistributionCenter::class)->create();

        // Set the initial levels
        App\Models\Inventory::create([
            'sku' => $product->sku,
            'distribution_center_id' => $distributionCenter->id,
            'count' => $initial,
        ]);

        $acutal = $this->inventoryRepository->incrementProductCountAtDistributionCenter($distributionCenter->id, $product->sku, $incrementBy);
        $this->assertEquals($expected, $acutal);
    }

    public function increment_inventory_levels_data_provider()
    {
        return [
            [0, 50, 50],
            [0, 0, 0],
            [1, 0, 1],
            [100, 1000, 1100],
            [0, -1, false], // cannot increment by negative
        ];
    }

    public function test_it_returns_false_on_invalid_sku_when_incrementing_inventory()
    {
        $distributionCenter = factory(App\Models\DistributionCenter::class)->create();

        $this->assertFalse($this->inventoryRepository->incrementProductCountAtDistributionCenter($distributionCenter->id, 1, 10));
    }

    public function test_it_returns_false_on_invalid_distribution_center_id_when_incrementing_inventory()
    {
        $product = factory(App\Models\Product::class)->create();

        $this->assertFalse($this->inventoryRepository->incrementProductCountAtDistributionCenter(1, $product->sku, 10));
    }

    /**  @dataProvider decrement_inventory_levels_data_provider **/
    public function test_it_decrements_inventory_levels_correctly($initial, $decrementBy, $expected)
    {
        $product = factory(App\Models\Product::class)->create();
        $distributionCenter = factory(App\Models\DistributionCenter::class)->create();

        // Set the initial levels
        App\Models\Inventory::create([
           'sku' => $product->sku,
           'distribution_center_id' => $distributionCenter->id,
           'count' => $initial,
        ]);

        $this->assertEquals($expected, $this->inventoryRepository->decrementProductCountAtDistributionCenter(
           $distributionCenter->id,
           $product->sku,
           $decrementBy
        ));
    }

    public function decrement_inventory_levels_data_provider()
    {
        return [
            [100, 50, 50],
            [0, 0, 0],
            [1, 0, 1],
            [999, 1000, false], // cannot decrement more than available
            [0, 1, false], // cannot decrement when nothing inside
            [100, -1, false], // cannot decrement by negative
        ];
    }

    /** @dataProvider available_inventory_levels_data_provider  */
    public function test_it_returns_available_inventory_correctly($inital, $incrementBy, $decrementBy, $expected)
    {
        $product = factory(App\Models\Product::class)->create();
        $distributionCenter = factory(App\Models\DistributionCenter::class)->create();

        // Set the initial levels
        App\Models\Inventory::create([
            'sku' => $product->sku,
            'distribution_center_id' => $distributionCenter->id,
            'count' => $inital,
        ]);

        $this->inventoryRepository->incrementProductCountAtDistributionCenter($distributionCenter->id, $product->sku, $incrementBy);
        $actual = $this->inventoryRepository->decrementProductCountAtDistributionCenter($distributionCenter->id, $product->sku, $decrementBy);

        $this->assertEquals($expected, $actual, 'Inventory levels are not correct');
    }

    public function available_inventory_levels_data_provider()
    {
        return [
          [100, 1, 10, 91],
          [0, 100, 100, 0],
          [0, 99, 55, 44],
        ];
    }

    /** @dataProvider  reservation_inventory_levels_data_provider */
    public function test_it_reserves_inventory_levels_correctly($initial, $reservationCount, $expectedReservedCount)
    {
        $product = factory(App\Models\Product::class)->create();
        $distributionCenter = factory(App\Models\DistributionCenter::class)->create();

        // Set the initial levels
        App\Models\Inventory::create([
            'sku' => $product->sku,
            'distribution_center_id' => $distributionCenter->id,
            'count' => $initial,
        ]);

        $actual = $this->inventoryRepository->reserveProductAtDistributionCenter($distributionCenter->id, $product->sku, $reservationCount);

        $this->assertEquals($expectedReservedCount, $actual, 'Reserved count is not as expected');
    }

    public function reservation_inventory_levels_data_provider()
    {
        return [
          [100, 99, 99],
          [99, 55, 55],
          [99, 0, 0],
        ];
    }

    public function test_it_fails_to_reserve_when_available_count_is_not_sufficient()
    {
        $product = factory(App\Models\Product::class)->create();
        $distributionCenter = factory(App\Models\DistributionCenter::class)->create();

        // Set the initial levels
        App\Models\Inventory::create([
            'sku' => $product->sku,
            'distribution_center_id' => $distributionCenter->id,
            'count' => 100,
        ]);

        $actual = $this->inventoryRepository->reserveProductAtDistributionCenter($distributionCenter->id, $product->sku, 1000);

        $this->assertFalse($actual,'Reservation has been done');
    }

    public function test_it_fails_to_reserve_negative_quantity()
    {
        $product = factory(App\Models\Product::class)->create();
        $distributionCenter = factory(App\Models\DistributionCenter::class)->create();

        // Set the initial levels
        App\Models\Inventory::create([
            'sku' => $product->sku,
            'distribution_center_id' => $distributionCenter->id,
            'count' => 100,
        ]);

        $actual = $this->inventoryRepository->reserveProductAtDistributionCenter($distributionCenter->id, $product->sku, -10);

        $this->assertFalse($actual,'Reservation has been done');
    }

    public function test_it_returns_available_count_correctly()
    {
        $product = factory(App\Models\Product::class)->create();
        $distributionCenter = factory(App\Models\DistributionCenter::class)->create();

        // Set the initial levels
        App\Models\Inventory::create([
            'sku' => $product->sku,
            'distribution_center_id' => $distributionCenter->id,
            'count' => 100,
        ]);

        $actual = $this->inventoryRepository->getAvailableInventoryLevel($distributionCenter->id, $product->sku);

        $this->assertEquals(100, $actual,'Wrong inventory availability level');
    }

    public function test_it_returns_reserved_count_correctly()
    {
        $product = factory(App\Models\Product::class)->create();
        $distributionCenter = factory(App\Models\DistributionCenter::class)->create();

        // Set the initial levels
        App\Models\Inventory::create([
            'sku' => $product->sku,
            'distribution_center_id' => $distributionCenter->id,
            'count' => 100,
            'reserved_count' => 20,
        ]);

        $actual = $this->inventoryRepository->reserveProductAtDistributionCenter($distributionCenter->id, $product->sku, 29);


        $actual = $this->inventoryRepository->getReservedInventoryLevel($distributionCenter->id, $product->sku);

        $this->assertEquals(49, $actual,'Wrong inventory availability level');
    }

}
