<?php

namespace Tests\Feature;

use Artisan;
use Tests\TestCase;

class InventoryReportTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        Artisan::call('db:seed');
    }

    public function tearDown()
    {
        Artisan::call('migrate:refresh');

        parent::tearDown();
    }

    public function test_it_can_generate_all_products_report()
    {
        // Add products
        $this->post('api/inventory/add', [
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => 100,
        ]);

        $this->post('api/inventory/add', [
            'distribution_center_id' => '2',
            'sku' => '1',
            'count' => 200,
        ]);

        $this->post('api/inventory/reserve', [
            'distribution_center_id' => '2',
            'sku' => '1',
            'count' => 200,
        ]);

        $this->post('api/inventory/add', [
            'distribution_center_id' => '1',
            'sku' => '2',
            'count' => 32,
        ]);

        $this->post('api/inventory/reduce', [
            'distribution_center_id' => '1',
            'sku' => '2',
            'count' => 32,
        ]);

        // Test the report of available products available across all distribution centers
        $response = $this->get('api/reports')->json()['data'];

        // Test that two items are present (one available, one reserved)
        $this->assertEquals(2, count($response));
    }

    public function test_it_can_generate_reserved_products_report_correctly_at_distribution_center_level()
    {
        // Add products
        $this->post('api/inventory/add', [
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => 100,
        ]);

        $this->post('api/inventory/add', [
            'distribution_center_id' => '2',
            'sku' => '1',
            'count' => 200,
        ]);

        // Reserve one product
        $this->post('api/inventory/reserve', [
            'distribution_center_id' => '2',
            'sku' => '1',
            'count' => 200,
        ]);

        // Assert that the product is found in response
        $this->get('api/reports/reserved', [
            'distribution_center_id' => '2'
        ])->assertJsonFragment([
            'distribution_center_id' => '2',
            'sku' => '1',
            'reserved_count' => "200",
            'count' => '0',
        ]);
    }

    public function test_it_can_generate_available_products_report_correctly_at_product_level()
    {
        // Add products
        $this->post('api/inventory/add', [
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => 100,
        ]);

        $this->post('api/inventory/add', [
            'distribution_center_id' => '2',
            'sku' => '1',
            'count' => 200,
        ]);

        // Reserve one product
        $this->post('api/inventory/reserve', [
            'distribution_center_id' => '2',
            'sku' => '1',
            'count' => 200,
        ]);

        // Assert that the product is found in response
        $response = $this->get('api/reports/available', [
            'sku' => '1'
        ]);

        $response->assertJsonFragment([
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => '100',
        ]);
    }

    public function test_it_can_generate_reserved_products_report_correctly_at_product_level()
    {
        // Add products
        $this->post('api/inventory/add', [
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => 100,
        ]);

        $this->post('api/inventory/add', [
            'distribution_center_id' => '2',
            'sku' => '1',
            'count' => 200,
        ]);

        $this->post('api/inventory/reserve', [
            'distribution_center_id' => '2',
            'sku' => '1',
            'count' => 200,
        ]);


        // Reserve one product
        $this->post('api/inventory/reserve', [
            'distribution_center_id' => '2',
            'sku' => '1',
            'count' => 200,
        ]);

        // Assert that the product is found in response
        $this->get('api/reports/reserved', [
            'sku' => '1'
        ])->assertJsonFragment([
            'distribution_center_id' => '2',
            'sku' => '1',
            'reserved_count' => "200",
            'count' => '0',
        ]);
    }

}
