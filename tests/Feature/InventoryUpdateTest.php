<?php

namespace Tests\Feature;

use Artisan;
use Tests\TestCase;

class InventoryUpdateTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        Artisan::call('db:seed');
    }

    public function tearDown()
    {
        Artisan::call('migrate:refresh');

        parent::tearDown();
    }

    public function test_inventory_can_be_increased()
    {
        $response = $this->post('api/inventory/add', [
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => 100,
        ])->json();

        $this->assertEquals(100, (int)$response['count'], 'Failed because count is wrong');
    }

    public function test_inventory_can_be_decreased()
    {
        // Add first
        $this->post('api/inventory/add', [
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => 100,
        ]);

        // Decrease and test
        $response = $this->post('api/inventory/reduce', [
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => 49,
        ]);

        $this->assertEquals(51, (int)$response->json()['count'], 'Failed because count is wrong');
    }

    public function test_inventory_decrease_will_be_forbidden_if_product_is_not_sufficiently_available()
    {
        // Add first
        $this->post('api/inventory/add', [
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => 100,
        ]);

        // Decrease and test
        $this->post('api/inventory/reduce', [
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => 101,
        ])
            ->assertStatus(403)
            ->assertJson([
                "error" => "Product is not sufficiently available"
            ]);
    }

    public function test_inventory_can_be_reserved()
    {
        // Add first
        $this->post('api/inventory/add', [
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => 100,
        ]);

        // Reserve and test
        $this->post('api/inventory/reserve', [
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => 30,
        ])
            ->assertStatus(200)
            ->assertJsonFragment([
                'count' => "70",
                'reserved_count' => "30",
            ]);
    }

    public function test_inventory_cannot_be_reserved_if_count_insufficient()
    {
        // Add first
        $this->post('api/inventory/add', [
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => 100,
        ]);

        // Reserve and test
        $this->post('api/inventory/reserve', [
            'distribution_center_id' => '1',
            'sku' => '1',
            'count' => 101,
        ])
            ->assertStatus(403)
            ->assertJson([
                "error" => "Product is not sufficiently available"
            ]);
    }

}
