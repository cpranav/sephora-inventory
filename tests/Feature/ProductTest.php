<?php

namespace Tests\Feature;

use Tests\TestCase;

class ProductTest extends TestCase
{
    public function test_it_can_add_a_product_correctly()
    {
        $product = factory(\App\Models\Product::class)->make();

        $response = $this->post('/api/products', [
            'name' => $product->name,
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'name' => $product->name,
            ]);
    }

    public function test_it_can_list_products_correctly()
    {
        $products = factory(\App\Models\Product::class, 5)->make();

        foreach ($products as $product) {
            $this->post('/api/products', [
                'name' => $product->name,
            ]);
        }

        $productNames = $products->pluck('name')->toArray();
        $listing = $this->get('api/products');
        $listingProductNames = array_column($listing->json('data'), 'name');

        $this->assertEquals($productNames, $listingProductNames, 'Product names are different');
    }

    public function test_it_can_show_a_single_product_correctly()
    {
        $products = factory(\App\Models\Product::class, 3)->make();

        foreach ($products as $product) {
            $this->post('/api/products', [
                'name' => $product->name,
            ]);
        }

        $listingProducts = $this->get('api/products')->json('data');

        foreach ($listingProducts as $listingProduct) {
            $showApi = $this->get('api/products/' . $listingProduct['sku'])->json();

            $this->assertEquals($listingProduct['name'], $showApi['name'], 'Correct product has not been fetched');
        }
    }

}
