#!/bin/bash

# Install composer dependencies
docker run --rm --interactive --tty \
    --volume $PWD:/app \
    --user $(id -u):$(id -g) \
    composer install

# Copy example env as main one
cp .env.example .env

# Set folder permissions
touch storage/logs/laravel.log
sudo chmod a+rw storage/logs/laravel.log
sudo chmod a+rwx -R storage/framework


# Start application
docker-compose build
docker-compose up -d

# Sleep for application to boot up
secs=$((20))
while [ $secs -gt 0 ]; do
   echo -ne "Waiting for application to bootup: $secs\033[0K\r"
   sleep 1
   : $((secs--))
done

# Generate key for laravel application
docker-compose exec app php artisan key:generate

# Run the migrations
docker-compose exec app php artisan migrate

# Run the seeder
docker-compose exec app php artisan db:seed

# Run tests
echo "Running tests"
docker-compose exec app ./vendor/bin/phpunit

# Show how to access
echo "The website is accessible at http://localhost:8080"
echo "phpMyAdmin is accessible at http://localhost:8081"
