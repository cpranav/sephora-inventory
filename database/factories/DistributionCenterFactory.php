<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\DistributionCenter::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
