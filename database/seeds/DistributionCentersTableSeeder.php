<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DistributionCentersTableSeeder extends Seeder
{

    /** @var Faker $faker */
    protected $faker;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\DistributionCenter::class)->create([
            'name' => 'Singapore',
        ]);

        factory(\App\Models\DistributionCenter::class)->create([
            'name' => 'Thailand',
        ]);
    }
}
