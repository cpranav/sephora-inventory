<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->unsignedInteger('distribution_center_id');
            $table->unsignedInteger('sku');
            $table->unsignedBigInteger('count')->default(0);
            $table->unsignedBigInteger('reserved_count')->default(0);

            $table->primary(['sku', 'distribution_center_id']);

            $table->foreign('distribution_center_id')->references('id')->on('distribution_centers');
            $table->foreign('sku')->references('sku')->on('products');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
