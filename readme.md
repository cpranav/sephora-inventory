## Requirements

- Docker (<a href="https://www.docker.com/community-edition">Download</a>)
- Docker Compose (<a href="https://docs.docker.com/compose/install/">Installation Instructions</a>)
- Environment to run shell scripts

## Running the project

Please run the bootstrap.sh bash shell script with the requirements installed.

Also please remember to keep the following ports free on the host system.

- 8080 for web project
- 8081 for phpMyAdmin

## List of APIs

The list of APIs is provided in the project home view in a browser.

## Useful links
- APIs are at <a href="http://localhost:8080">http://localhost:8080</a>
- phpMyAdmin is at <a href="http://localhost:8081">http://localhost:8081</a>

## Tests 
- Unit tests in **tests/Unit** folder
- Higher level integration tests for APIs in **tests/Feature** folder

## Built with
- Laravel 5.6
- mySQL
- SQLite for testing
- Docker for automation 