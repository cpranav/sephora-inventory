<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** Products related */
Route::get('products', 'ProductController@index');
Route::get('products/{sku}', 'ProductController@show');
Route::post('products', 'ProductController@store');

/** Inventory changes */
Route::post('inventory/add', 'InventoryUpdateController@add');
Route::post('inventory/reduce', 'InventoryUpdateController@reduce');
Route::post('inventory/reserve', 'InventoryUpdateController@reserve');

/** Inventory reports */
Route::get('reports', 'InventoryReportController@index');
Route::get('reports/available', 'InventoryReportController@showAvailable');
Route::get('reports/reserved', 'InventoryReportController@showReserved');
