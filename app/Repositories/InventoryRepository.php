<?php

namespace App\Repositories;


use App\Models\DistributionCenter;
use App\Models\Inventory;
use App\Models\Product;
use DB;

class InventoryRepository
{
    private $product;
    private $distributionCenter;
    private $inventory;


    public function __construct(Product $product, DistributionCenter $distributionCenter, Inventory $inventory)
    {
        $this->product = $product;
        $this->distributionCenter = $distributionCenter;
        $this->inventory = $inventory;
    }

    public function incrementProductCountAtDistributionCenter($distributionCenterId, $sku, $incrementBy = 1)
    {
        if (! $this->isDistributionCenterIdValid($distributionCenterId) || ! $this->isProductSkuValud($sku)) {
            return false;
        }

        if ($incrementBy < 0) {
            return false;
        }

        DB::beginTransaction();

        $inventory = $this->inventory->firstOrCreate([
            'distribution_center_id' => $distributionCenterId,
            'sku' => $sku,
        ]);

        $inventory->count = $inventory->count + $incrementBy;
        $inventory->save();

        DB::commit();

        return $inventory->count;
    }

    public function decrementProductCountAtDistributionCenter($distributionCenterId, $sku, $decrementBy = 1)
    {
        if (! $this->isDistributionCenterIdValid($distributionCenterId) || ! $this->isProductSkuValud($sku)) {
            return false;
        }

        if ($decrementBy < 0) {
            return false;
        }

        DB::beginTransaction();

        $inventory = $this->inventory->firstOrCreate([
            'distribution_center_id' => $distributionCenterId,
            'sku' => $sku,
        ]);

        if ($inventory->count - $decrementBy < 0) {
            return false;
        }

        $inventory->count = $inventory->count - $decrementBy;
        $inventory->save();

        DB::commit();


        return $inventory->count;
    }

    public function reserveProductAtDistributionCenter($distributionCenterId, $sku, $reservationCount = 1)
    {
        if (! $this->isDistributionCenterIdValid($distributionCenterId) || ! $this->isProductSkuValud($sku)) {
            return false;
        }

        if ($reservationCount < 0) {
            return false;
        }

        DB::beginTransaction();

        $inventory = $this->inventory->firstOrCreate([
            'distribution_center_id' => $distributionCenterId,
            'sku' => $sku,
        ]);


        if ($inventory->count - $reservationCount < 0) {
            return false;
        }

        $inventory->count =  $inventory->count - $reservationCount;
        $inventory->reserved_count =  $inventory->reserved_count + $reservationCount;
        $inventory->save();

        DB::commit();

        return $inventory->reserved_count;
    }

    public function getAvailableInventoryLevel($distributionCenterId, $sku = null)
    {
        if (! $this->isDistributionCenterIdValid($distributionCenterId) || ! $this->isProductSkuValud($sku)) {
            return false;
        }

        $inventory = $this->inventory->where('distribution_center_id', $distributionCenterId)
                                     ->where('sku', $sku)
                                     ->first();

        return $inventory ? $inventory->count : 0;
    }


    public function getReservedInventoryLevel($distributionCenterId, $sku = null)
    {
        if (! $this->isDistributionCenterIdValid($distributionCenterId) || ! $this->isProductSkuValud($sku)) {
            return false;
        }

        $inventory = $this->inventory->where('distribution_center_id', $distributionCenterId)
                                     ->where('sku', $sku)
                                     ->first();

        return $inventory ? $inventory->reserved_count : 0;
    }

    private function isDistributionCenterIdValid($id)
    {
        return $this->distributionCenter->find($id) ? true : false;
    }

    private function isProductSkuValud($sku)
    {
        return $this->product->find($sku) ? true : false;
    }

}
