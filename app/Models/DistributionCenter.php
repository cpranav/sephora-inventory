<?php

namespace App\Models;

class DistributionCenter extends BaseModel
{

    protected $fillable = ['name'];

    public function inventory()
    {
        $this->hasMany('App\Models\Inventory');
    }

}
