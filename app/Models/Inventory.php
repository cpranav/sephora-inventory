<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Builder;

class Inventory extends BaseModel
{
    protected $fillable = ['sku', 'distribution_center_id', 'count', 'reserved_count'];

    protected function setKeysForSaveQuery(Builder $query)
    {
        $query->where('sku', '=', $this->getAttribute('sku'))
              ->where('distribution_center_id', '=', $this->getAttribute('distribution_center_id'));

        return $query;
    }

}
