<?php

namespace App\Models;


class Product extends BaseModel
{

    protected $primaryKey = 'sku';

    protected $fillable = ['name'];

    public function inventory()
    {
        $this->hasMany('App\Models\Inventory', 'sku', 'sku');
    }

}
