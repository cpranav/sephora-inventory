<?php

namespace App\Controllers;

use App\Models\Inventory;
use App\Repositories\InventoryRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Validator;


class InventoryUpdateController extends BaseController
{

    /** @var InventoryRepository */
    private $inventoryRepository;

    public function __construct(InventoryRepository $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
    }

    public function add(Request $request)
    {
        $validationRules = [
            'distribution_center_id' => 'required|exists:distribution_centers,id',
            'sku' => 'required|exists:products,sku',
            'count' => 'int|min:0',
        ];

        $validator = Validator::make($request->all(), $validationRules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 403);
        }

        return response()->json([
            'sku' => $request->sku,
            'distribution_center_id' => $request->distribution_center_id,
            'count' => $this->inventoryRepository->incrementProductCountAtDistributionCenter($request->distribution_center_id, $request->sku, $request->count),
        ]);
    }

    public function reduce(Request $request)
    {
        $validationRules = [
            'distribution_center_id' => 'required|exists:distribution_centers,id',
            'sku' => 'required|exists:products,sku',
            'count' => 'int|min:0',
        ];

        $validator = Validator::make($request->all(), $validationRules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 403);
        }

        $reducedCount = $this->inventoryRepository->decrementProductCountAtDistributionCenter($request->distribution_center_id, $request->sku, $request->count);

        if ($reducedCount === false) {
            return response()->json([
                'error' => 'Product is not sufficiently available',
            ], 403);
        }

        // Fetch the inventory model
        $inventory = Inventory::where('sku', $request->sku)
            ->where('distribution_center_id', $request->distribution_center_id)
            ->first();

        return response()->json($inventory);
    }

    public function reserve(Request $request)
    {
        $validationRules = [
            'distribution_center_id' => 'required|exists:distribution_centers,id',
            'sku' => 'required|exists:products,sku',
            'count' => 'int|min:0',
        ];

        $validator = Validator::make($request->all(), $validationRules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 403);
        }

        $reservedCount = $this->inventoryRepository->reserveProductAtDistributionCenter($request->distribution_center_id, $request->sku, $request->count);

        if ($reservedCount === false) {
            return response()->json([
                'error' => 'Product is not sufficiently available',
            ], 403);
        }

        // Fetch the inventory model
        $inventory = Inventory::where('sku', $request->sku)
            ->where('distribution_center_id', $request->distribution_center_id)
            ->first();

        return response()->json($inventory);
    }

}
