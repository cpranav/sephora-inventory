<?php

namespace App\Controllers;

use App\Models\Inventory;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;


class InventoryReportController extends BaseController
{
    public function index()
    {
        return response()->json(
            Inventory::where('count', '>', 0)->orWhere('reserved_count', '>', 0)->paginate()
        );
    }

    public function showAvailable(Request $request)
    {
        $query = Inventory::select();

        if ($request->distribution_center_id) {
            $query->where('disribution_center_id', $request->distribution_center_id);
        }

        if ($request->sku) {
            $query->where('sku', $request->sku);
        }

        return response()->json(
            $query->where('count', '>',0)->paginate()
        );
    }

    public function showReserved(Request $request)
    {
        $query = Inventory::select();

        if ($request->distribution_center_id) {
            $query->where('disribution_center_id', $request->distribution_center_id);
        }

        if ($request->sku) {
            $query->where('sku', $request->sku);
        }

        return response()->json(
            $query->where('reserved_count', '>', 0)->paginate()
        );
    }

}