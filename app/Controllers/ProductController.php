<?php

namespace App\Controllers;

use App\Models\Product;
use App\Validators\StoreProduct;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

class ProductController extends BaseController
{
    use  ValidatesRequests;

    public function index()
    {
        return Product::paginate();
    }

    public function store(StoreProduct $request)
    {
        return response()->json(Product::create($request->validated()));
    }

    public function show($sku)
    {
        return response()->json(Product::find($sku));
    }
}
