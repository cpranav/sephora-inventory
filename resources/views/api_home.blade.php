<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Simple Inventory Management System: Sephora Programming Assignment</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #000;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 60px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .table {
            color: #000;
            font-size: 14px;
            font-family: SFMono-Regular, Menlo, Monaco, Consolas, Liberation Mono, Courier New, monospace;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            Simple Inventory Management System
        </div>
        <table class="table-responsive table">
            <thead>
            <tr>
                <th>
                    API
                </th>
                <th>
                    URI
                </th>
                <th>
                    Params
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    1.) Add a new product
                </td>
                <td>
                    POST /api/products
                </td>
                <td>
                    - name: String of max length 255 chars
                </td>
            </tr>
            <tr>
                <td>
                    2.) Get a product by it's SKU
                </td>
                <td>
                    GET /api/products/{sku}
                </td>
                <td>
                    -
                </td>
            </tr>
            <tr>
                <td>
                    3.) Get a list of all products
                </td>
                <td>
                    GET /api/products
                </td>
                <td>
                    -
                </td>
            </tr>
            <tr>
                <td>
                    4.) Increase inventory
                </td>
                <td>
                    POST /api/inventory/add
                </td>
                <td>
                    - distribution_center_id (required)<br/>
                    - sku (required)<br/>
                    - count (optional) : Defaults to 1<br/>
                </td>
            </tr>
            <tr>
                <td>
                    5.) Decrease inventory
                </td>
                <td>
                    POST /api/inventory/reduce
                </td>
                <td>
                    - distribution_center_id (required)<br/>
                    - sku (required)<br/>
                    - count (optional) : Defaults to 1<br/>
                </td>
            </tr>
            <tr>
                <td>
                    6.) Reserve inventory
                </td>
                <td>
                    POST /api/inventory/reserve
                </td>
                <td>
                    - distribution_center_id (required)<br/>
                    - sku (required)<br/>
                    - count (optional) : Defaults to 1<br/>
                </td>
            </tr>
            <tr>
                <td>
                    7.) Get global inventory report
                </td>
                <td>
                    GET /api/reports
                </td>
                <td>
                  -
                </td>
            </tr>
            <tr>
                <td>
                    8.) Get available inventory report - scoped to distribution center / sku level
                </td>
                <td>
                    GET /api/reports/available
                </td>
                <td>
                    - distribution_center_id (optional) : To scope it to a distribution center level <br/>
                    - sku (optional) : To scope it to product level
                </td>
            </tr>
            <tr>
                <td>
                    9.) Get pending shipped inventory report : scoped to distribution center / sku level
                </td>
                <td>
                    GET /api/reports/reserved
                </td>
                <td>
                    - distribution_center_id (optional) : To scope it to a distribution center level<br/>
                    - sku (optional) : To scope it to product level
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
